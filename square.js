const axios = require('axios');

const config = require('./config.json').development;
const baseUrl = "https://connect.squareup.com/v2";
const uuidv4 = require('uuid/v4');

const square = axios.create({
  baseURL: baseUrl,
  headers: {
    'Content-type': 'application/json',
    'Authorization': `Bearer ${config.squareAccessToken}`,
    'Accept': 'application/json'
  }
});

/* Location API */
module.exports.locations = {};
module.exports.locations.findLocation = async () => {
  const body = (await square.get('/locations')).data;
  const locations = body.locations;
  for (var i = locations.length - 1; i >= 0; i--) {
    location = locations[i];
    if (location.capabilities && location.capabilities.indexOf("CREDIT_CARD_PROCESSING") > -1) {
      return location
    }
  }
  return new Error("No locations have credit card processing available.")
}

/* Customer API */
module.exports.customers = {};
module.exports.customers.create = async customerData => {
  console.log('Creating Customer');
  const customer = await square.post('/customers', customerData);
  return customer.data.customer;
}

module.exports.customers.fetchById = async customerId => {
  console.log(`Fetching Customer id: ${customerId}`);
  const customer = await square.get(`/customers/${customerId}`);
  return customer.data.customer;
}

module.exports.customers.list = async () => {
  console.log('Listing customers');
  const body = (await square.get('/customers')).data;
  const customers = body.customers;
  return customers;
}

/* Charges API */
module.exports.charges = {};
module.exports.charges.create = async (locationId, chargeData) => {
  console.log('Creating Charge!');
  chargeData.idempotency_key = uuidv4();
  try {
    const charge = await square.post(`/locations/${locationId}/transactions`, chargeData);
    let transaction = charge.data.transaction;
    transaction.idempotency_key = chargeData.idempotency_key;
    return transaction;
  }
  catch (e) {
    console.log('Error creating charge!')
    let error = new Error('Error Creating Charge')
    error.idempotency_key = chargeData.idempotency_key;
    error.originalError = e;
    throw error;
  }
}

module.exports.charges.list = async locationId => {
  console.log('Listing Charges');
  const body = (await square.get(`/locations/${locationId}/transactions`)).data;
  const charges = body.transactions;
  return charges;
}