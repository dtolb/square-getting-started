const square = require('./square')

const getFirstLocation = async () => {
  try {
    const location = await square.locations.findLocation();
    // console.log(location);
    return location;
  }
  catch (e) {
    console.log(e);
    process.exit(1);
  }
}

const addCustomer = async () => {
  const faker = require('faker');
  const customerData = {
    "given_name" : faker.name.findName(),
    "phone_number": faker.phone.phoneNumber(),
    "email_address": faker.internet.email(),
  }
  try {
    const createdCustomer = await square.customers.create(customerData);
    // console.log(createdCustomer);
    const fetchedCustomer = await square.customers.fetchById(createdCustomer.id)
    // console.log(fetchedCustomer)
    return createdCustomer;
  }
  catch (e) {
    console.log(e);
    process.exit(1);
  }
}

const listCustomers = async () => {
  try {
    const listedCustomers = await square.customers.list();
    console.log(listedCustomers)
    return listedCustomers;
  }
  catch (e) {
    console.log(e);
    process.exit(1);
  }
}

const createCharge = async () => {
  const location = await getFirstLocation();
  const customer = await addCustomer();
  const chargeData = {
  "shipping_address": {
    "address_line_1": "123 Main St",
    "locality": "San Francisco",
    "administrative_district_level_1": "CA",
    "postal_code": "94114",
    "country": "US"
  },
  "billing_address": {
    "address_line_1": "500 Electric Ave",
    "address_line_2": "Suite 600",
    "administrative_district_level_1": "NY",
    "locality": "New York",
    "postal_code": "10003",
    "country": "US"
  },
  "amount_money": {
    "amount": 999,
    "currency": "USD"
  },
  "card_nonce": "fake-card-nonce-ok",
  "reference_id": "some optional reference id",
  "note": "some optional note",
  "delay_capture": false
  }
  chargeData.customer_id = customer.id;
  try {
  	const charge = await square.charges.create(location.id, chargeData);
  	console.log(charge);
  	return charge;
  }
  catch (e) {
  	console.log(e);
  }
}

const listCharges = async () => {
	const location = await getFirstLocation();
	try {
		const listedCharges = await square.charges.list(location.id);
		console.log(listedCharges);
		return listedCharges;
	}
	catch (e) {
		console.log(e);
		process.exit(1);
	}
}

// getFirstLocation();
// addCustomer();
// listCustomers();
// createCharge();
// listCharges();
